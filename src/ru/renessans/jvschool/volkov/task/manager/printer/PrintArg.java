package ru.renessans.jvschool.volkov.task.manager.printer;

import ru.renessans.jvschool.volkov.task.manager.constant.ArgConst;

final public class PrintArg implements ArgConst {

    public void print(final String[] args) {
        if (isEmptyArgs(args)) {
            printMsg(NO_ARG);
            return;
        }

        final String arg = args[0];
        printByArg(arg);
    }

    private boolean isEmptyArgs(final String[] args) {
        return args == null || args.length < 1;
    }

    private void printByArg(final String arg) {
        switch (arg) {
            case HELP_ARG: {
                printMsg(String.format(FORMAT_HELP_MSG, VERSION_ARG, ABOUT_ARG, HELP_ARG));
                break;
            }
            case VERSION_ARG: {
                printMsg(VERSION_MSG);
                break;
            }
            case ABOUT_ARG: {
                printMsg(String.format(FORMAT_ABOUT_MSG, DEVELOPER, DEVELOPER_MAIL));
                break;
            }
            default: {
                printMsg(String.format(FORMAT_UNKNOWN_MSG, arg));
                break;
            }
        }
    }

    private void printMsg(final String msg) {
        System.out.println(msg);
        System.exit(0);
    }

}